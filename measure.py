#coding: utf-8
import serial
import struct
import collections
import contextlib
import datetime
import logging
import binascii
import io
import os
import codecs

import jinja2

logging.basicConfig(level=logging.INFO)

config = {
    'port': '/dev/ttyAMA0',
    'baud': 9600,
    'data_format': '>4xh3ih'
}

attributes = ("water", "temp1" ,"temp2", "pressure" , "humidity", "timestamp")

@contextlib.contextmanager
def open_reader(port, baud, data_format):
    def reader(connection):
        while True:
            line = connection.readline()[:-2] # strip CRLF

            if not line:
                raise StopIteration
            timestamp = datetime.datetime.now()
            logging.info("Read %d bytes: %r" % (len(line), binascii.hexlify(line)))

            try:
                values = struct.unpack(data_format, line)
            except struct.error:
                logging.info("Received invalid data, skipping datapoint")
                continue
            else:
                int_values = tuple(int(value) for value in values)

                data = [
                int_values[0] / 100.0,
                int_values[1] / 10.0,
                int_values[2] / 10.0,
                int_values[3] / 100.0,
                int_values[4] / 10.0
                ]

                yield DataPoint(timestamp, attributes, data)

    connection = serial.Serial(port, baud)
    try:
        yield reader(connection)
    finally:
        connection.close()


class DataPoint:

    def __init__(self, timestamp, attributes, values):
        self.timestamp = timestamp
        self.attributes, self.values = attributes, values

    def return_maximum(self, max_values):
        new_max_values = []
        for new, old in zip(self.values, max_values):
            new_max_values.append(max(new, old))

        return new_max_values  

    def return_minimum(self, min_values):
        new_min_values = []
        for new, old in zip(self.values, min_values):
            new_min_values.append(min(new, old))

        return new_min_values

    def get_log_line(self):
        return "{} {}\n".format(self.timestamp.isoformat(), " ".join(str(value) for value in self.values))

    def __repr__(self):
        rows = ["\t{}: {}".format(key, value) for key, value in zip(self.attributes, self.values)]
        return "{}:\n{}".format(self.timestamp.isoformat(), "\n".join(rows))


class HtmlGenerator:

    def __init__(self, aliases, units, template):
        self.aliases = aliases
        self.units = units

        self.template = template

    def generate(self, values, minimum_values, maximum_values, timestamp):
        value_rows = zip(values, minimum_values, maximum_values)
        rows = zip(self.aliases, self.units, value_rows)
        return self.template.render(rows=rows, timestamp=timestamp.strftime("%d.%m.%Y %H:%M:%S"))

    def generate_to_file(self, destination, values, minimum_values, maximum_values, timestamp):
        with codecs.open(destination, "w", "utf-8") as out:
            out.write(self.generate(values, minimum_values, maximum_values, timestamp))

    @staticmethod
    def load_template(template_file, directory=None):
        if not directory:
            directory = os.getcwd()
        environment = jinja2.Environment(loader=jinja2.FileSystemLoader(directory))
        return environment.get_template(template_file)


if __name__ == "__main__":
    aliases = [
        'Wassertemperatur',
        'Temperatur 1',
        'Temperatur 2',
        'Luftdruck',
        'Luftfeuchtigkeit'
    ]
    units = [
        u'°C',
        u'°C',
        u'°C',
        u'hPa',
        u'%'
    ]

    minimum_values = None
    maximum_values = None

    html_generator = HtmlGenerator(aliases, units, HtmlGenerator.load_template("template.html"))

    with open_reader(config['port'], config['baud'], config['data_format']) as reader:
        # reader ist ein iterables objekt, das datenpunkte yieldet
        for datapoint in reader:
            print(datapoint)

            if not minimum_values:
                minimum_values = datapoint.values
            if not maximum_values:
                maximum_values = datapoint.values

            minimum_values = datapoint.return_minimum(minimum_values)
            maximum_values = datapoint.return_maximum(maximum_values)

            with open(datapoint.timestamp.date().strftime("%Y_%m_%d") + ".dat", "a") as out:
                out.write(datapoint.get_log_line())

            html_generator.generate_to_file("index.html", datapoint.values, minimum_values, maximum_values, datapoint.timestamp)


